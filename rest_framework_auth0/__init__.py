# -*- coding: utf-8 -*-
from django.conf import settings as django_settings

__title__ = 'djangorestframework-auth0'
__version__ = '0.2.0'
__author__ = 'Marcelo Cueto'
__license__ = 'MIT'
__copyright__ = 'Copyright 2015-2016 Marcelo Cueto'

# Version synonym
VERSION = __version__

default_app_config = 'rest_framework_auth0.apps.DjangorestframeworkAuth0Config'

if 'constance' in django_settings.INSTALLED_APPS:
    constance_additional_fields = getattr(django_settings, 'CONSTANCE_ADDITIONAL_FIELDS', {})
    if 'drf_auth0' not in getattr(django_settings, 'CONSTANCE_ADDITIONAL_FIELDS', {}):
        constance_additional_fields.update({'drf_auth0': ['rest_framework_auth0.fields.JSONFormField']})
        setattr(django_settings, 'CONSTANCE_ADDITIONAL_FIELDS', constance_additional_fields)
