from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from rest_framework.settings import APISettings

try:
    from constance import config
    USER_SETTINGS = config.AUTH0
except (ImportError, AttributeError, ImproperlyConfigured):
    USER_SETTINGS = getattr(settings, 'AUTH0', None)

DEFAULTS = {
    'clients': {
        'AUTH0_CLIENT_ID': {
            'AUTH0_CLIENT_SECRET': '',
            'AUTH0_ALGORITHM': 'HS256',
            # 'AUTHORIZATION_EXTENSION': True,
            # 'DISABLED': True,
            # 'CLIENT_SECRET_BASE64_ENCODED': False
            # 'USERNAME_FIELD': 'user',
        }
    },
    'USERNAME_FIELD': 'sub',
    'CLIENT_SECRET_BASE64_ENCODED': True,
}

# List of settings that may be in string import notation.
IMPORT_STRINGS = ()

api_settings = APISettings(USER_SETTINGS, DEFAULTS, IMPORT_STRINGS)
