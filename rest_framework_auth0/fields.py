# coding: utf-8
try:
    from jsoneditor.fields.django_jsonfield import JSONFormField as _JSONFormField

    class JSONFormField(_JSONFormField):
        def __init__(self, *args, **kwargs):
            self.load_kwargs = kwargs.pop('load_kwargs', {})
            super(JSONFormField, self).__init__(*args, **kwargs)
except ImportError:
    from django.forms import CharField as JSONFormField
