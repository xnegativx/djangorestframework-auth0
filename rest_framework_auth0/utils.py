import base64
import jwt

from rest_framework_auth0.settings import api_settings
from rest_framework_jwt.settings import api_settings as jwt_api_settings


# Handlers --------------------------------------------------------------------

def jwt_get_username_from_payload_handler(payload):
    client = api_settings.clients[payload['aud']]
    username = payload.get(client.get('USERNAME_FIELD', api_settings.USERNAME_FIELD))
    return username

# Authorization Utils ---------------------------------------------------------


def jwt_decode_handler(token):
    # read the claim first without signature validation
    payload = jwt.decode(token, verify=False)
    try:
        # use the settings for the audience provided
        client = api_settings.clients[payload['aud']]
        if client.get('DISABLED', False):
            raise jwt.InvalidTokenError
    except KeyError:
        # either the audience provided is not declared either the token as no audience set
        # anyway send an invalid token in these cases
        raise jwt.InvalidTokenError
    # exp validation can de desactivated as a global settings via JWT settings,it is not recomended...
    options = {
        'verify_exp': jwt_api_settings.JWT_VERIFY_EXPIRATION,
    }
    # Replace rest_framework_jwt api settings
    if client.get('CLIENT_SECRET_BASE64_ENCODED', api_settings.CLIENT_SECRET_BASE64_ENCODED):
        secret = base64.b64decode(client['AUTH0_CLIENT_SECRET'].replace("_", "/").replace("-", "+"))
    else:
        secret = client['AUTH0_CLIENT_SECRET']
    return jwt.decode(
        token,
        secret,
        jwt_api_settings.JWT_VERIFY,
        options=options,
        leeway=jwt_api_settings.JWT_LEEWAY,
        audience=payload['aud'],
        issuer=client.get('JWT_ISSUER', jwt_api_settings.JWT_ISSUER),
        algorithms=[client['AUTH0_ALGORITHM']]
    )

# Auth0 Metadata --------------------------------------------------------------


def get_app_metadata_from_payload(payload):
    app_metadata = payload.get('app_metadata')
    return app_metadata


def get_user_metadata_from_payload(payload):
    user_metadata = payload.get('user_metadata')
    return user_metadata


# Role validation utils -------------------------------------------------------

def get_roles_from_payload(payload):
    roles = get_app_metadata_from_payload(payload)['roles']
    return roles


def validate_role(roles, role):
    return role.upper() in roles


def validate_role_from_payload(payload, role):
    roles = get_roles_from_payload(payload)
    return validate_role(roles, role)


# Group validation utils ------------------------------------------------------

def get_groups_from_payload(payload):
    groups = get_app_metadata_from_payload(payload)['authorization']['groups']
    return groups


def validate_group(group, expected_group):
    return group == expected_group


def validate_group_from_payload(payload, expected_group):
    groups = get_groups_from_payload(payload)
    return expected_group in groups
