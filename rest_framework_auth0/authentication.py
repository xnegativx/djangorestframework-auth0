from django.contrib.auth.models import Group
from django.contrib.auth.backends import RemoteUserBackend, get_user_model
from django.utils.translation import ugettext as _

from rest_framework import exceptions
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.settings import api_settings as jwt_api_settings

from rest_framework_auth0.settings import api_settings
from rest_framework_auth0.utils import get_groups_from_payload

jwt_get_username_from_payload = jwt_api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER


class Auth0JSONWebTokenAuthentication(JSONWebTokenAuthentication, RemoteUserBackend):
    """
    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string specified in the setting
    `JWT_AUTH_HEADER_PREFIX`. For example:

        Authorization: JWT eyJhbGciOiAiSFMyNTYiLCAidHlwIj

    By default, the ``authenticate_credentials`` method creates ``User`` objects for
    usernames that don't already exist in the database.  Subclasses can disable
    this behavior by setting the ``create_unknown_user`` attribute to
    ``False``.
    """

    request = None

    def authenticate(self, request):
        # reference to request object
        self.request = request
        return super(Auth0JSONWebTokenAuthentication, self).authenticate(request)

    def authenticate_credentials(self, payload):
        """
        Returns an active user that matches the payload's user id and email.
        """
        # attach decoded payload to the request object
        self.request._decoded_jwt = payload

        remote_user = jwt_get_username_from_payload(payload)
        if not remote_user:
            msg = _('Invalid payload.')
            raise exceptions.AuthenticationFailed(msg)
        user = RemoteUserBackend.authenticate(self, remote_user)
        if not user:
            msg = _('Invalid signature.')
            raise exceptions.AuthenticationFailed(msg)
        user = self.configure_user_permissions(user, payload)
        return user

    def configure_user_permissions(self, user, payload):
        """
        Validate if AUTHORIZATION_EXTENSION is enabled, defaults to False

        If AUTHORIZATION_EXTENSION is enabled, created and associated groups
        with the current user (the user of the token).
        """
        if api_settings.clients[payload['aud']].get('AUTHORIZATION_EXTENSION', False):
            user.groups.clear()
            try:
                groups = get_groups_from_payload(payload)
            except Exception:  # No groups where defined in Auth0?
                return user
            for user_group in groups:
                group, created = Group.objects.get_or_create(name=user_group)
                user.groups.add(group)

        return user

    def clean_username(self, username):
        """
        Cleans the "username" prior to using it to get or create the user object.
        Returns the cleaned username.

        Auth0 default username (user_id) field returns, e.g. auth0|123456789...xyz
        which contains illegal characters ('|').
        """
        username = username.replace('|', '.')
        return username
